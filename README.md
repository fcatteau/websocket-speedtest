
[![pipeline status](https://gitlab.com/dansiviter/websocket-speedtest/badges/master/pipeline.svg)](https://gitlab.com/dansiviter/websocket-speedtest/commits/master) [![coverage report](https://gitlab.com/dansiviter/websocket-speedtest/badges/master/coverage.svg)](https://gitlab.com/dansiviter/websocket-speedtest/commits/master)

# WebSocket Speed Test #

A little Java, React and Material UI app to perform some calculations on the websocket connection between browser and server.

<img src="images/screenshot.png" alt="Echo UI" width="550"/>